D. The udevadm command is used to work with the udev interface of the kernel, and the monitor subcommand displays kernel uevents and other udev events in real time.





B. Current IRQ assignments are contained in the file /proc/interrupts. Therefore, viewing the contents of the file with a command such as cat will work. There is no “view” command, making option A incorrect. Likewise, there is no /dev/irq file, making options C and D incorrect.





D. Configuration files for udev are found in /etc/udev, which makes option D correct. The other options do not exist.





A. The modprobe command loads the module and its dependencies, if applicable. The lsmod command is used to list currently loaded modules, making option B incorrect. The insmod command will load a given module but not its dependencies. Option D, rmmod, is used to remove a module from memory.





B. The lsusb command is used to obtain a basic list of USB devices on a system. The other commands are not valid. In the case of option D, the ls command is valid, but there is no --usb option.





B. The info command for udevadm enables querying for additional information about a hotplug device managed with udev.





D. The dmesg command displays the contents of the kernel ring buffer. On many Linux distributions, this log is also saved to /var/log/dmesg. The other options shown for this question are not valid commands.





C. Runlevel 1, sometimes displayed as runlevel s or S, is single-user mode in which many services are not started. Runlevels 5 and 6 are used for other purposes, and runlevel SU is not a valid option.





D. Scripts are stored in /etc/init.d on a system using SysV init. You may sometimes find these linked from /etc/rc.d/init.d as well. The other options are not valid for this question.





A. The init command can be used to access different runlevels. Runlevel 6 is used for rebooting the system. Option B will shut down the system entirely, not reboot it. Option C will place the system into single-user mode. Option D is not a valid option.





C. The telinit command can be used to refresh the system after changes have been made to /etc/inittab. Notably, option B will reboot the system but that was not an option in the question. Options A and D are not valid commands.





D. The runlevel command displays the current runlevel for a system. Option B is not a valid option to the init command, and adding sudo in front of the init command makes no difference. Option A is not a valid command.





C. Unit configuration files are stored in /lib/systemd/system. The other directory options for this question are not relevant or do not exist by default.





B. The systemctl command is used to work with services and targets. The list-units command is used to list targets. The other commands are not used for this purpose or do not exist with the required option.





C. The -nn option displays both numbers and device names, making option C correct. The -n option (option B) displays only numbers. The other two options do not exist.





D. The lsmod command is used to list currently loaded kernel modules, making option D correct for this question. The insmod command (option A) is used to load modules. Option C is a valid command but not a valid option for that command, and option B does not exist.





C. The --show-depends option shows the modules that depend on the specified module. The other options do not exist.





B. The wall command is used to send a message to all users, thereby making option B correct. The cat command is used as a means to concatenate or view files, and tee is used to send output to standard output and a file. Finally, ssh is the secure shell client command and is not used for the purpose specified.





B. Checking to ensure that the disk is detected in the basic input/output system (BIOS) is a good first step in troubleshooting. Option A, unplugging the disk, won’t help it to be detected. Restarting the web server won’t help detect the disk, and the disk-detect command does not exist.





D. The /sys/bus/usb/devices directory contains information about USB devices. The other directories are not valid for this purpose.





D. The file /var/log/dmesg will typically contain historical messages from the current booting of the system. On some distributions of Linux, this information is also in /var/log/boot.log.





C. Out of the options given, the systemctl status command and option is the most appropriate. The telinit and sysctl commands are not used for this purpose. Likewise, the --ls option is not valid for systemctl.





B. The isolate option is used to move the system into the target specified, making option B the correct one. The other options do not exist.





A. The initctl reload command causes Upstart to reread its configuration files.





B. The --list option will show all services on a system along with their status for each runlevel.





C. USB devices are generally considered to be hotplug devices. Hotplug devices describe those devices that can be inserted and removed while the system is “hot,” or powered on, whereas coldplug devices are those that must be inserted and removed when the system is powered off.





B. The umount command is used to unmount drives within a running system. The other commands do not exist.





D. Of the options presented, running dmesg is a common way to determine the location to which the kernel has assigned the drive. Rebooting the system is not a good option, though it would work. There is no such thing as /var/log/usb.log, and the location of the drive may change regardless of port, depending on how the drive may be detected in the system.





B. From these options, only B will shut down the system immediately. Option A will cancel a shutdown.





C. The ExecStart option indicates the command to be executed on startup of a systemd service.





D. The systemctl get-default command will show the default target. The other commands and options are not valid.





A. The enable option configures the service to start on boot. The start option, D, is used to start a service immediately. The other options are not valid for this command.





C. The /proc filesystem contains information about currently running processes and additional information about the kernel and current boot of the system.





C. The -t option to lsusb will print output in a tree-like format so that you can see which devices are connected to which bus. The other arguments to lsusb are not valid, and the usblist command is not real.





D. If a working device does not appear in lsmod, it typically means that the kernel has a driver already loaded by virtue of being compiled into the kernel itself rather than loaded through a module. The use of systemd (option A) or initramfs (option B) has no effect.





C. The -w option causes the module to wait until it’s no longer needed prior to unloading. The -f option forces immediate removal and should be used with caution. The other options are not valid for rmmod.





B. The tune2fs command can be used for this purpose but should be used with care because it can result in data corruption.





C. Rules related to udev are stored in /etc/udev/rules.d. The /etc/udev hierarchy contains the udev.conf configuration file along with other components related to the configuration of udev.





B. The -k option shows the kernel driver associated with a given PCI device and can be helpful when planning a new kernel compile. The -t option displays information in a tree-like structure, and -n uses numbers instead of device names. There is no -a option.





B. The /etc/modprobe.d directory is used for storing configuration information related to modules such as that used for blacklisting purposes but also for other configuration information, such as udev and module options.





B. The dracut command is used to create the initial RAM disk for newer systems and has replaced the legacy mkinitrd command used for the same purpose.





D. The file /proc/kallsyms provides a way to view the currently loaded kernel symbols. This can be helpful for resolving module dependencies. Note that on legacy systems, this file might be called /proc/ksyms.





A. The systool utility can be used to show currently loaded options for a given module. The modinfo -r command is not valid, and though modinfo shows information about a module, it does not include core size and other settings. The lsmod command cannot be used for this purpose, and there is no infmod command.





B. The /proc/sys/kernel hierarchy contains vital configuration information about a kernel. These settings can be changed on a running system.





B. The /etc/systemd/system directory is where it is recommended to store unit files for systemd. The other locations are not valid.





C. The systemctl command will be used for this purpose with the daemon-reload subcommand. The reboot option would work to reload the systemd configuration but is not correct because it requires the entire server to reboot, which is not what was asked in this question.





B. The /etc/inittab file contains the various runlevels and what to run at the given runlevel. For example, runlevel 1 is single-user, runlevel 6 is reboot, and so on. The other files listed do not exist.





B. The SYSLINUX boot loader is used for FAT filesystems to create rescue disks and to assist with installation of Linux in general. SYSLINUX also describes an overall project containing other specialty boot loaders. The other options listed for this question are not valid boot loaders, though.





C. initrd is used for an initial root filesystem for early drivers. initrd is configured to load within the GRUB configuration file for a given operating system.





B. The fsck command is used to diagnose and repair hard drive problems in Linux. The defrag command is not available in Linux.





D. The telinit command can be used for this purpose, and passing 1 as the argument will switch the system into single-user mode. The other commands shown are not valid.





D. The -n option changes the boot order for the next boot only and boots from the specified partition. The -b along with -B modifies and then deletes the option. The -o option sets the boot order. The -c option creates a boot number.





A. ISOLINUX provides a means by which CD-ROMS formatted as ISO 9660 can be booted. It’s very common to have live CDs or rescue/recovery CDs that use ISOLINUX for boot. The other boot loaders are not valid for this purpose or don’t exist.





A. The /usr/lib/systemd hierarchy contains files related to systemd configuration. The user directory within the hierarchy is used for user unit files, and the system files are stored in /usr/lib/systemd/system.





B. Due to the decidedly insecure decisions made with the design of Microsoft’s UEFI, a shim is often needed to enable Linux to boot on a system with UEFI. The shim.efi file can be used as an initial boot loader for this purpose.





D. Scripts for starting and stopping services are located in /etc/init.d on a SysV init-based system. The other directories listed within this question are not valid.





C. The systemd-delta command is used to determine overridden configuration files. Of the other commands, diff is valid but not for this purpose. The systemctl command is also valid, but again, not for the purpose described.





B. The chkconfig --list command displays all services that will be executed on boot along with the setting for each service for each runlevel. Of the other commands, the init command is valid but does not have a --bootlist option. The other commands are invalid.





B. The bcfg command within the UEFI shell is used to configure boot loaders on a UEFI-based system. The command can accept various parameters to configure how the boot loader and kernel will load on boot. Of the other commands shown, grub-install is valid but not within the UEFI shell.





D. The pxelinux.0 file must exist within /tftpboot on the TFTP server in order for a system to use PXELINUX for booting. The other files are not valid or necessary for PXELINUX.





D. The update-rc.d utility can be used to manage SysV init scripts on Debian or Ubuntu and other distributions. When using update-rc.d, you supply the script name and the utility will take care of creating symlinks to the appropriate runlevels.





B. The e key, when pressed at the right time during boot, will send you into the GRUB shell, where you can change parameters related to boot, such as the kernel options and other related parameters.





D. The isolate subcommand followed by the desired target is used to switch between runlevels with a systemd-based system. The other subcommands shown are not valid for systemctl.





C. The runlevel defined as initdefault is the default runlevel for the system. The other options shown do not exist.





B. The initramfs system is used instead of initrd to create the filesystem-based loading process for key drivers that are needed for boot.





A. The systemctl command will be used for this purpose, and the set-default subcommand is necessary to affect the desired behavior. The target file is simply called multi-user.target.





C. The shim.efi boot loader loads another boot loader, which is grubx64.efi by default. The other options are not valid filenames for the purpose described.





D. The /etc/rc.d hierarchy contains symbolic links to files found within /etc/init.d. These symlinks are then used for executing the scripts at the appropriate runlevel. For example, on boot the system will execute the scripts found in the runlevel directory for each runlevel executed at boot time.





A. The default.target is the default target unit that is activated by systemd on boot. The default target then starts other services based on the dependencies.





B. LUNs that contain the characters fc are found through Fibre Channel. Therein lies the difference between options B and C, where option C contains the letters scsi, which would usually represent a local disk. The other options are not valid.





B. NVMe-capable drives are named /dev/nvme*. No special drivers are needed other than those found in the native kernel on a modern system. The other options do not exist as paths by default.





D. The /proc/mdstat file contains information on RAID arrays, including RAID personalities found on the system, the devices that comprise the array, and other pertinent information. The other files shown are not valid.





B. The /sys/class/fc_host directory contains other directories based on the Fibre Channel connections available. Within those host directories will be found the WWN in a file called port_name. The other directory hierarchies are not valid.





C. The /dev/mapper directory contains information about multipath devices such as logical volumes. The other directories are not valid.





C. The lspci command will be used for this purpose. NVMe devices are listed with the name nVME or NVMe; therefore, adding -i to grep will make the search case insensitive. You’d use this in order to ensure that the devices are detected. The other commands are not valid, with the exception of the lspci command, but you cannot grep for scsi in this scenario.





D. Tape devices are found within /dev/st*, making st0 the first device.





C. The /etc/issue file is used to provide a message to users, such as a login banner, prior to local login. The other files shown are not valid for the purpose described.





C. The contents of the file motd, an abbreviation for Message of the Day, are displayed when a user logs in successfully. Among the other options, the contents of /etc/issue are displayed prior to local login. The other filenames are not valid for this purpose.





B. The /etc/issue.net file is used to provide a message for remote logins such as telnet. The other files listed are not valid for the purpose described.





D. The poweroff target of systemd, accessed using the systemctl command, is used for halting the system and then attempting to remove power on compatible systems. The halt target stops the system but does not attempt to remove power, whereas reboot simply restarts the system. There is no stop target.





A. The -r option is needed to specify reboot, and the format for counting time from now is prefaced with a plus sign (+), making option A correct. Of the other commands, specifying +15 without the -r option simply shuts down the computer in 15 minutes, and specifying the time as 00:15, as in option D, will shut down the computer at 12:15 a.m.





A. The service command is used to work with services, such as starting and stopping them. On newer systems, the systemctl command has replaced the service command.





A. The journalctl command with the -b option displays boot messages.





A. The -h option halts the system, including shutting down acpid-related hardware.





C. The number 9 corresponds to SIGKILL and can be passed to the kill command to issue that signal. The number 1 is SIGHUP. Others can be found within the manual for the kill command.





C. The /etc/init.d directory contains the startup and shutdown scripts for services on a Debian system that is not running systemd.





A. Among the options, examining the boot messages would be a first logical step and would prevent having to reboot the system. Rebooting may be a next step in order to examine the status of the peripheral within the BIOS.





C. The -n option prevents the banner from displaying when using wall. The other options shown are not used with the wall command.

