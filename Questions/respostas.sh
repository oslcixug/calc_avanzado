#!/bin/bash

n=1
while read linea; do

	r=${linea:0:1}

	case $r in
	A)
		rp=$(((($n*8))-6))
		sed -i "${rp}s/^./=/" salida_good.txt
	;;
	B)	
		rp=$(((($n*8))-5))
		sed -i "${rp}s/^./=/" salida_good.txt
	;;
	C)
		rp=$(((($n*8))-4))
		sed -i "${rp}s/^./=/" salida_good.txt
	;;
	D)
		rp=$(((($n*8))-3))
		sed -i "${rp}s/^./=/" salida_good.txt
	;;
	esac
	
	rp=$(((($n*8))-2))
	sed -i "${rp}s/^.*$/###<p>${linea:3}<br><\/p>/" salida_good.txt
	echo "${rp}s/^.*$/###<p>${linea:3}<br><\/p>/"


	n=$((n+1))

done < $1



